vm_name=$(gcloud compute instances list --format=json | jq -r '.[].name')

read -p "Delete $vm_name? (Y/n) " del_bool

if test "$del_bool" = "n"
then
	exit
fi

gcloud compute instances delete $vm_name
