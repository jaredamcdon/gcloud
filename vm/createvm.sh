vm_name="test-instance"

image=$(gcloud compute images list | grep debian-11)

image=$(echo $image | awk '{print $1}')

echo "Image: $image"

gcloud compute instances create $vm_name \
	--image=$image \
	--image-project=debian-cloud \
	--machine-type=e2-micro \
	--zone=us-central1

# alternate 
#gcloud compute instances create $vm_name \
#	--image-family=debian-11
#	--image-project=debian-cloud
#	--machine-type=
